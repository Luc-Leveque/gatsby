import React from "react"
  
import { Link } from "gatsby"
import "./success.css"


const Success = () => {
  return (
    <div class="success-page">
      <h2 class="header_sucess">Action Réussie !</h2>
      <Link
        to="/products"
      >
        Continuer les Achats <br/> 
      </Link>
      <Link
        to="/"
      >
        Retourner à l'accueil
      </Link>
    </div>
  )
}

export default Success