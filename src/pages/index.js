import React from "react"
import { Link } from "gatsby"

import { graphql } from 'gatsby'

import Carousel from 'react-bootstrap/Carousel';
import Button from 'react-bootstrap/Button';

import Layout from "../components/Layout.js"
import Identity from "../components/Identity.js"
import { useNetlifyIdentity } from "react-netlify-identity-widget"

export const query = graphql`
    query newProduct {
      allMarkdownRemark(sort: {order: ASC, fields: frontmatter___date}) {
        nodes {
          frontmatter {
            categorie
            description
            image
            path
            price
            title
            date
          }
        }
      }
    }
`


const IndexPage = ({data}) => {

console.log(data);

  const firstItem = data.allMarkdownRemark.nodes[0] 
  const midItem = data.allMarkdownRemark.nodes[1]
  const lastItem  = data.allMarkdownRemark.nodes[2] 


  return (
    <Layout>
      <h1 className="h1__index">Nouveautés</h1>
      <section className="carousel__index">
        <Carousel>
          <Carousel.Item>
            <Link
                to={"/"+firstItem.frontmatter.path}
                className="bar"
              >
                <img className="carousel" src={firstItem.frontmatter.image} alt=""/>
            </Link>
          </Carousel.Item>
          <Carousel.Item>
            <Link
                to={"/"+midItem.frontmatter.path}
                className="bar"
            >
              <img  className="carousel" src={midItem.frontmatter.image} alt=""/>
            </Link>
          </Carousel.Item>
          <Carousel.Item>
            <Link
                to={"/"+lastItem.frontmatter.path}
                className="bar"
              >
                <img className="carousel" src={lastItem.frontmatter.image} alt=""/>
            </Link>
          </Carousel.Item>
        </Carousel>
      </section>
    </Layout>
  )
}

export default IndexPage