import React from "react"
import Layout from "../../components/Layout.js"

const Contact = () => {
  return (
    <Layout>
      <section class="mb-4">

        <h2 class="h1-responsive font-weight-bold text-center my-4">Contactez nous</h2>

        <div class="row">
          <div class="col-md-12 mb-md-0 mb-5">
            <form id="contact" name="contact" method="post" netlify-honeypot="bot-field" data-netlify="true" action="/success">
              <input type="hidden" name="form-name" value="contact" />
              <div class="row">
                <div class="col-md-6">
                    <div class="md-form mb-0">
                      <input type="text" id="name" name="name" class="form-control"/>
                      <label for="name" class="">Votre Nom</label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="md-form mb-0">
                      <input type="text" id="email" name="email" class="form-control"/>
                      <label for="email" class="">Votre Email</label>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                    <div class="md-form mb-0">
                      <input type="text" id="subject" name="subject" class="form-control"/>
                      <label for="subject" class="">Objet</label>
                    </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="md-form">
                    <textarea type="text" id="message" name="message" rows="2" class="form-control md-textarea"></textarea>
                    <label for="message">Votre Message</label>
                  </div>
                </div>
              </div>
              <div class="text-center text-md-left">
                <button class="btn btn-primary" type="submit">Send</button>
              </div>
            </form>
          </div>
        </div>
      </section>
    </Layout>
    
  )
}

export default Contact;

