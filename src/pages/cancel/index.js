import React from "react"

import { Link } from "gatsby"
import "./cancel.css"

const Cancel = () => {
  return (
    <div class="success-page">
    <h2 class="header_cancel">L'action a échoué ! Veuillez réessayer ultérieurement</h2>
    <Link
      to="/products"
    >
      Continuer les Achats <br/> 
    </Link>
    <Link
      to="/"
      className="link__cancel"
    >
      Retourner à l'accueil
    </Link>
  </div>
  )
}

export default Cancel