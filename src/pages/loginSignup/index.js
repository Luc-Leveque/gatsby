import React, { useState } from 'react';
const netlifyIdentity = require('netlify-identity-widget');


const Index =  () => {

    netlifyIdentity.open();

    // Get the current user:
    const user = netlifyIdentity.currentUser();
    console.log(user);
    
    // Bind to events
    netlifyIdentity.on('init', user => console.log('init', user));
    netlifyIdentity.on('login', user => console.log('login', user));
    netlifyIdentity.on('logout', () => console.log('Logged out'));
    netlifyIdentity.on('error', err => console.error('Error', err));
    netlifyIdentity.on('open', () => console.log('Widget opened'));
    netlifyIdentity.on('close', () => console.log('Widget closed'));
    
    // Close the modal
    netlifyIdentity.close();
    
    // Log out the user
    netlifyIdentity.logout();

    return (
        <div>
            <head>
                <title>A static website</title>

                <script type="text/javascript" src="https://identity.netlify.com/v1/netlify-identity-widget.js"></script>
            </head>

            <div data-netlify-identity-menu></div>

            <div data-netlify-identity-button>Login with Netlify Identity</div>

        </div>
    );
}

export default Index;
