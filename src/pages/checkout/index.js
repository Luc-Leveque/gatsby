import React, { useState } from 'react';
import { loadStripe } from '@stripe/stripe-js';


const Index =  () => {

    const stripePromise =  loadStripe('pk_test_WFITMq8sFep6HETOP3qQ0xZ100LYXWFJ7F');

    const checkButton = async (e) => {
        e.preventDefault();

        const stripe = await stripePromise ;
        stripe
        .redirectToCheckout({
          items: [
            // Replace with the ID of your SKU
            {sku: 'sku_H9mcIO5xKLSwsL', quantity: 1},
          ],
          successUrl: 'https://stoic-ardinghelli-3657ef.netlify.app/success',
          cancelUrl: 'https://stoic-ardinghelli-3657ef.netlify.app/cancel',
        })
        .then(function(result) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer
          // using `result.error.message`.
        });
    }

    return (
        <div>
            <form>
                <button onClick={checkButton}>
                    Payer
                </button>
            </form>
        </div>
    );
}

export default Index;
