import React, { Component } from "react"

import { graphql } from 'gatsby'
import Layout from "../components/Layout.js"

import Checkout from "../components/Checkout"

import Image from 'react-bootstrap/Image';

export default ({data}) => {
  return (
    <Layout>
      <div id="page-content" class="container">
          <section id="project">
              <div class="row">
                  <div class="col-sm-12">
                      <h3>{data.markdownRemark.frontmatter.title}</h3>
                      <div class="project-content-area">
                        <div class="row">
                          <div class="col-sm-12">
                            <Image className="image image__product" src={data.markdownRemark.frontmatter.image} fluid />
                          </div>
                        </div>
                          <div class="row">

                              <div class="col-sm-4">
                                  <h4>INFORMATIONS</h4>
                                  <div class="project-info">
                                      <div class="info">
                                          <p><span>Prix : <b>{data.markdownRemark.frontmatter.price + " euros"}</b></span></p>
                                      </div>
                                      <div className={data.markdownRemark.frontmatter.couleur ? `show` : `hide`}>
                                        <p><span>Couleur : <b>{data.markdownRemark.frontmatter.couleur}</b></span></p>
                                      </div>
                                      <div className={data.markdownRemark.frontmatter.taille ? `show` : `hide`}>
                                        <p><span>Taille : <b>{data.markdownRemark.frontmatter.taille}</b></span></p>
                                      </div>
                                  </div>
                              </div>

                              <div class="col-sm-8">
                                  <h4>DESCRIPTION DU PRODUIT</h4>
                                  <p>{data.markdownRemark.frontmatter.description}</p>
                              </div>
                          </div>
                          <Checkout sku={data.markdownRemark.frontmatter.sku}/>
                      </div>
                  </div> 
              </div>
          </section>
      </div>
    </Layout>
  )
}

export const query = graphql`
    query($path: String!) {
        markdownRemark(frontmatter: {path: {eq: $path}}) {
            frontmatter {
              description
              image
              taille
              couleur
              path
              price
              title
              date
              sku
            }
          }
    }
`
