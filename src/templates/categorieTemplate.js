import React, { Component } from "react"

import { graphql } from 'gatsby'
import { Link } from "gatsby"

import Card from 'react-bootstrap/Card';
import CardDeck from 'react-bootstrap/CardDeck';
import Button from 'react-bootstrap/Button';

import Layout from "../components/Layout.js"


export default ({data}) => {
  return (
    <Layout>
      <CardDeck>
        {
          data.allMarkdownRemark.edges.map((product) => {
            return <Card className="col-3" style={{ width: '18rem' }}>
                      <Card.Img className="card__img" variant="top" src={product.node.frontmatter.image}/>
                      <Card.Body>
                        <Card.Title>{product.node.frontmatter.title}</Card.Title>
                        <Card.Text>
                        <div>{product.node.frontmatter.price} €</div><br/>
                        <div>{product.node.frontmatter.description}</div>
                        <Link
                            to={"/"+ product.node.frontmatter.path}
                            className="bar"
                          >
                            <Button variant="primary">Détail du Produit</Button>
                        </Link>

                        </Card.Text>
                      </Card.Body>
                    </Card>
        
          })
        }
      </CardDeck>
    </Layout>
  )
}


export const query = graphql`
    query($categorie: String!){
        allMarkdownRemark(filter: {frontmatter: {categorie: {eq: $categorie}}}) {
            edges {
                node {
                    frontmatter {
                        description
                        image
                        price
                        title
                        path
                    }
                }
            }
        }
    }
`