import React, { useState } from "react"
import PropTypes from "prop-types"

import { useNetlifyIdentity } from "react-netlify-identity-widget"
import IdentityModal from "react-netlify-identity-widget"

const Identity = () => {
  const [showDialog, setShowDialog] = useState(false)

  const identity = useNetlifyIdentity(
    "https://stoic-ardinghelli-3657ef.netlify.app/"
  )

  return identity && identity.user ? (
    <>
      <p>Hello {identity.user.user_metadata.full_name}</p>
    </>
  ) : <>
  <button class="btnLogin" onClick={() => setShowDialog(true)}>Log In</button> 
    <IdentityModal
      className="position"
      showDialog={showDialog}
      onCloseDialog={() => setShowDialog(false)}
    />
  </>
}

export default Identity