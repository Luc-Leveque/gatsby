/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import "react-netlify-identity-widget/styles.css"

import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import {
  useNetlifyIdentity,
  IdentityContextProvider,
} from "react-netlify-identity-widget"
import Header from "./Header"
import "./Layout.css"


const Layout = ({ children }) => {
  const identity = useNetlifyIdentity(
    "https://stoic-ardinghelli-3657ef.netlify.app/"
  )

  return (
    <>
      <StaticQuery
        query={graphql`
          query SiteTitleQuery {
            site {
              siteMetadata {
                title
              }
            }
          }
        `}
        render={data => (
          <IdentityContextProvider value={identity}>
            <Header siteTitle={data.site.siteMetadata.title} />
            <div
              class="main"
            >
              <main>{children}</main>
            </div>
          </IdentityContextProvider>
        )}
      />
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout