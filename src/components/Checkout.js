import React, { useState } from 'react';
import { loadStripe } from '@stripe/stripe-js';


const Index = props => {

    const stripePromise =  loadStripe('pk_test_WFITMq8sFep6HETOP3qQ0xZ100LYXWFJ7F');

    const checkButton = async (e) => {
        e.preventDefault();

        const stripe = await stripePromise ;
        stripe
        .redirectToCheckout({
          items: [
            // Replace with the ID of your SKU
            {sku: props.sku, quantity: 1},
          ],
          successUrl: 'http://localhost:8000/success',
          cancelUrl: 'http://localhost:8000/cancel',
        })
        .then(function(result) {
          // If `redirectToCheckout` fails due to a browser or network
          // error, display the localized error message to your customer
          // using `result.error.message`.
        });
    }

    return (
        <div>
            <form>
                <button onClick={checkButton}>
                    Payer
                </button>
            </form>
        </div>
    );
}

export default Index;
