
import 'bootstrap/dist/css/bootstrap.min.css';


import { Link } from "gatsby"
import PropTypes from "prop-types"
import React, { useState } from "react"
import IdentityModal from "react-netlify-identity-widget"
import Identity from "../components/Identity.js"

import "./Header.css"
import logo from "../../static/image/logo-badenbaden.png"


const Header = ({ siteTitle }) => {
  

  return (
      <header class = "container">
        <div class="row mb-2 header">
          <Link
          className="logo_header"
            to="/"
          > 
            <div>
              <img src={logo} alt="Logo" />
            </div> 
            
            <div>
                <p class="blue logo">SITE</p>
                <p class="blue logo">ECOMMERCE</p>
                <p class="logo">  de badenbaden </p>
            </div>
          </Link>

          <nav id="menu" class="nav-screen-sup navbar__header">
              <ul>
                  <li class="pipe">
                  <Link
                      to="/products"
                      className="bar"
                    >
                      PRODUITS
                    </Link>
                  </li>
                  <li class="pipe"><a href="#" class="bar">CATALOGUE</a>
                      <ul>
                          <li>
                            <Link
                              to="/Vetement"
                              className="ss_bar"
                            >
                              VETEMENT
                            </Link>
                          </li>
                          <li>
                            <Link
                              to="/Multimedia"
                              className="ss_bar"
                            >
                              MULTIMEDIA
                            </Link>
                          </li>
                      </ul>
                  </li>
                  <li class="pipe">
                    <Link
                        to="/contact"
                        className="bar"
                      >
                        CONTACT
                    </Link>
                  </li>
                  <Identity/>
              </ul>
          </nav>
        </div>
      </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header