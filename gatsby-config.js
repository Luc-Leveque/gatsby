/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `Gatsby`,
    siteUrl: `https://www.gatsbyjs.org`,
    description: `Blazing fast modern site generator for React`,
  },
  /* Your site config here */
  plugins: [
    `gatsby-plugin-netlify-cms`,
    `gatsby-transformer-remark`,
    `gatsby-transformer-sharp`,
    'gatsby-plugin-netlify-identity-widget',
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-netlify-identity`,
      options: {
        url: `https://stoic-ardinghelli-3657ef.netlify.app/` // required!
      }
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `product`,
        path: `${__dirname}/products`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `articles`,
        path: `${__dirname}/articles`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/pages`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `categorie`,
        path: `${__dirname}/categorie`,
      },
    },
    {
      resolve: "gatsby-plugin-compile-es6-packages",
      options: {
        modules: ["react-netlify-identity-widget"],
      },
    },
   //{
   //  resolve: `gatsby-source-filesystem`,
   //  options: {
   //    name: `images`,
   //    path: `${__dirname}/src/images`,
   //  },
   //},
    //{
    //  resolve: `gatsby-source-filesystem`,
    //  options: {
    //    name: `data`,
    //    path: `${__dirname}/src/data/`,
    //    ignore: [`**/\.*`], // ignore files starting with a dot
    //  },
    //},
  ],
}
