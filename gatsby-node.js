exports.createPages = async ({ actions, graphql }) => {
    const products = await graphql(`
        query {
            allMarkdownRemark (filter: {fileAbsolutePath: {regex: "/products/"}}) {
                edges {
                    node {
                        frontmatter {
                            path
                        }
                    }
                }
            }
        }
    `)

    products.data.allMarkdownRemark.edges.forEach( edge => {
        const path = edge.node.frontmatter.path;
        console.log(path);
        if(path){
            actions.createPage({
                path: path,
                component: require.resolve('./src/templates/productTemplate.js'),
                context: {
                    path:path
                }
            })
        }
    })

   const categorie = await graphql(`
        query {
            allMarkdownRemark (filter: {fileAbsolutePath: {regex: "/categorie/"}}) {
                edges {
                    node {
                        frontmatter {
                            path
                            id
                        }
                    }
                }
            }
        } 
    `)

    categorie.data.allMarkdownRemark.edges.forEach( edge => {

        const id = edge.node.frontmatter.id;
        const path = edge.node.frontmatter.path;
        if(path){
            actions.createPage({
                path: path,
                component: require.resolve('./src/templates/categorieTemplate.js'),
                context: {
                    categorie:id
                }
            })
        }
    })
    

};