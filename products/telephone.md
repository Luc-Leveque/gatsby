---
title: Telephone
price: "10"
description: >-
  * Caller ID/Call Waiting

  * Speakerphone, Audio Assist

  * Extra-Large Tilt Display, LCD Display, Hearing aid compatible

  * Extra-Large Buttons, Extra-Loud Ringer. Table- and wall-mountable

  * DSL Subscribers may need to use a DSL Filter. Line power speakerphone

  * The telephone works without AC Power. For the Caller ID to function, four AA batteries (not included) are necessary
image: /assets/telephone.jpg
date: May 27, 2020 4:26 PM
sku: sku_H9mcIO5xKLSwsL
path: telephone
categorie: 389d3f03-3fa3-54fd-9279-131cbbac92c8
---
